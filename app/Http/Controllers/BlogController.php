<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// alternatively, use another PSR-4 compliant autoloader
use Faker;
class BlogController extends Controller
{
    public function index()
    {

    }

    public function show()
    {
        $faker = Faker\Factory::create();

        $data = array();

        for($i= 0 ; $i<10; $i++)
        {
            //$datos = array('name' => $faker->name, 'text' => $faker->text);
            array_push($data, $faker->name, $faker->text);
        }
        //return view('posts.index', ['tittle' => 'El titulo', 'data' => $data]);

    }

    public function email(Request $request)
    {
        $datos = $request->all();
        return view('email', ['name' => $datos['name'],
            'msj' => $datos['msj']]);
    }
}
