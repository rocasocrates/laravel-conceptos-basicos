<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['id', 'post_id', 'email', 'comment', 'isResponse', 'commment_id', 'created_at', 'updated_at'];

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
    public function comment()
    {
        return $this->belongsTo('App\Models\Comment');
    }
}
