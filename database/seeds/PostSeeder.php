<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert(['id' => 0,
            'title' => "La durísima represión a los mineros de Río Tinto que desafiaron a
                                                Queipo de Llano",
            'article' => 'La fosa común de Nerva inicia un largo proceso de exhumación dónde podría albergarse entre 221 y 500 víctimas de la represión golpista. La mayoría son mineros, hombres que lucharon al inicio del golpe militar para frenar la entrada del fascismo de Queipo de Llano. Les costó demasiado caro. En 1937 ya se registraban en Nerva doscientas cuarenta y seis viudas y quinientos setenta niños huérfanos.',
            'created_at' => '2018-10-10',
            'updated_at' => '2018-10-11'

        ]);

        DB::table('posts')->insert(['id' => 1,
            'title' => "La contundente respuesta de Sherpa (Barón Rojo) a una solicitud del programa La Voz Senior",
            'article' => 'José Luis Campuzano ‘Sherpa’, excomponente de Barón Rojo y actualmente en Los Barones, ha compartido públicamente su contundente respuesta a una solicitud del programa televisivo \'La Voz Senior\': Hola, amigos. Me acaban de mandar un email del programa La Voz Senior, diciendo que les gustaría contar conmigo como concursante, porque les viene bien tener algún concursante rockero de más de 60 años. Eso sí, tendría que hacer una audición a ver si a Bisbal, Paulina Rubio y otros les parece que doy la talla para su equipo.',
            'created_at' => '2018-10-10',
            'updated_at' => '2018-10-11'

        ]);
        DB::table('posts')->insert(['id' => 2,
            'title' => "Greenpeace apunta a OKdiario como uno de los portales que más bulos propaga",
            'article' => 'Con la ayuda de un equipo de voluntarios, la asociación ecologista y pacifista ha investigado
             qué bulos han circulado por diferentes listas de difusión de WhatsApp analizando más de 300 mensajes
              "desinformativos", que hacían referencia a cinco grandes temas: partidos políticos, nacionalismos,
               xenofobia, género, islamofobia, ONG y sistema electoral.',
            'created_at' => '2018-10-10',
            'updated_at' => '2018-10-11'

        ]);


    }
}
