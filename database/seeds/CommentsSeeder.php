<?php

use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert(['id' => 0,
            'post_id' => 0,
            'email' => 'prueba@gmail.com',
            'comment' => 'Llegará el día del apocalipsis zombie y Público sacará en
                                         portada algún episodio de la Guerra Civil.',
            'isResponse' => true,
            'comment_id' => 0,
            'created_at' => '2018-05-05',
            'updated_at' => '2018-05-05']);

        DB::table('comments')->insert(['id' => 1,
            'post_id' => 0,
            'email' => 'prueba@gmail.com',
            'comment' => '¡Hostia! ... 500 asesinados en un pueblo, no me extraña que los hijos de puta no quieran
             que se saquen de las cunetas y vean el tamaño del horror.',
            'isResponse' => true,
            'comment_id' => 0,
            'created_at' => '2018-05-05',
            'updated_at' => '2018-05-05']);
    }
}
