<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/contacto', 'ContactController@index')->name('contacto');
Route::get('/tickets', 'PostController@index')->name('tickets');
Route::get('/completeTickets/{id?}', 'PostController@show')->name('complete-tickets');
Route::post('/contact/email', 'BlogController@email')->name('email');
Route::get('users/comment/{id}', function($id){

    return view('comments.create', ['id' => $id]);

});
Route::post('/create/comment/', 'CommentController@store');

Route::get('/adult', ['middleware' => 'age:18', function () {
    return "eres mayor de edad y puedes ver este contenido";
}]);

/*Route::get('/adult', ['middleware' => 'age:18', function () {
    return "eres mayor de edad y puedes ver este contenido";
}]);*/